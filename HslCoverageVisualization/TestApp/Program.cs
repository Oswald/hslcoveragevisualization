﻿using HslCoverageVisualization.Contracts.Gateways.TransportDataGateway;
using HslCoverageVisualization.Contracts.Storage;
using HslCoverageVisualization.Core.UseCases;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TestApp
{
    class StopData
    {
        public double latitude;
        public double longitude;
        public double time;
        public Guid id;
        public string name;
    }
    class Program
    {
        static void Main(string[] args)
        {
            var storage = new TransportDataStorage("Server=192.168.1.3;Port=5432;User Id=hsl;Password=L978y93gfasfhwq;Database=hsl;");
            var gateway = new CacheTransportDataGateway(storage);
            var interactor = new CalculateTransportTimesToStops(gateway);
            var s = new Stopwatch();
            s.Start();
            var data = interactor.Handle(new DateTime(2019, 11, 25, 10, 0, 0), new Guid("1437f155-98cf-405a-a041-1d38dabe6656"));
            Console.WriteLine(s.ElapsedMilliseconds);
            foreach (var item in data)
            {
                Console.WriteLine($"{item.Key.Name}: {item.Value}");
            }

            List<StopData> stops = data.Select(d => new StopData { latitude = d.Key.Location.Latitude, longitude = d.Key.Location.Longitude, time = d.Value, id = d.Key.Id, name = d.Key.Name }).ToList();

            Console.Clear();
            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(stops));
        }

    }
}
