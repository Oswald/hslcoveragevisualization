﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Core.Entities
{
    public class Location
    {
        public double Latitude { get; }
        public double Longitude { get; }

        public Location(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }
    }
}
