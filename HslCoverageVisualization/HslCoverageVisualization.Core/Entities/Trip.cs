﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Core.Entities
{
    public class Trip
    {
        public Stop Stop { get; }
        public int ArrivalTime { get; }
        public int DepartureTime { get; }

        public Trip(Stop target, int departure,  int arrivalTime)
        {
            Stop = target;
            ArrivalTime = arrivalTime;
            DepartureTime = departure;
        }
    }
}
