﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Core.Entities
{
    public class Line
    {
        public Guid Id { get; }
        public string Name { get; }

        public Line(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
