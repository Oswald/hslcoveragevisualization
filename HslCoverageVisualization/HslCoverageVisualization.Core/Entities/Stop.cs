﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Core.Entities
{
    public class Stop
    {
        public Guid Id { get; }
        public string Name { get; }
        public Location Location { get; }

        public Stop(Guid id, string name, Location location)
        {
            Id = id;
            Name = name;
            Location = location;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
