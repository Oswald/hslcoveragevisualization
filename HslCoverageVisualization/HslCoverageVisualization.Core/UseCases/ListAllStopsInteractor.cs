﻿using HslCoverageVisualization.Core.Entities;
using HslCoverageVisualization.Core.Gateways;
using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Core.UseCases
{
    public class ListAllStopsInteractor
    {
        ITransportDataGateway TransportDataGateway { get; }
        public ListAllStopsInteractor(ITransportDataGateway transportDataGateway)
        {
            TransportDataGateway = transportDataGateway;
        }

        public List<Stop> Handle()
        {
            return TransportDataGateway.GetStops();
        }
    }
}
