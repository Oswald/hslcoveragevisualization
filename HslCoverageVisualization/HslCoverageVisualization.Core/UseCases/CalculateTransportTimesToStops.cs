﻿using HslCoverageVisualization.Core.Entities;
using HslCoverageVisualization.Core.Gateways;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace HslCoverageVisualization.Core.UseCases
{
    public class CalculateTransportTimesToStops
    {
        class StopNode
        {
            public Stop Stop { get; }
            public int Visited { get; }
            public int VisitedIdex { get; }

            public StopNode(Stop stop, int visited, int visitedIndex)
            {
                Stop = stop;
                Visited = visited;
                VisitedIdex = visitedIndex;
            }
        }

        class StopNodeComparer : IComparer<StopNode>
        {
            public int Compare(StopNode x, StopNode y)
            {
                if(x.Visited < y.Visited)
                {
                    return -1;
                }
                else if(x.Visited > y.Visited)
                {
                    return 1;
                }
                else
                {
                    if(x.VisitedIdex == y.VisitedIdex)
                    {
                        return 0;
                    }
                    if(x.VisitedIdex < y.VisitedIdex)
                    {
                        return -1;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
        }

        //Set of stops with visit time. Stop with smallest visit time is always handled next. 
        List<Stop> AllStops { get; } = new List<Stop>();
        SortedSet<StopNode> SortedNodes { get; } = new SortedSet<StopNode>(new StopNodeComparer());
        Dictionary<Stop, StopNode> Visited { get; } = new Dictionary<Stop, StopNode>();

        //To make the sortedSet work if there are multiple stops with the same visited time
        Dictionary<Stop, int> VisitedIndex = new Dictionary<Stop, int>();
        HashSet<StopNode> RemovedFromSorted { get; } = new HashSet<StopNode>();
        ITransportDataGateway TransportDataGateway { get; }
        DateTime Date { get; set; }
        public CalculateTransportTimesToStops(ITransportDataGateway transportDataGateway)
        {
            TransportDataGateway = transportDataGateway;
        }

        public Dictionary<Stop, int> Handle(DateTime date, Guid stopId)
        {
            //Pitää hoitaa niin, että pysäkiltä ei voida lähteä enää niillä linjoilla, joilla on jo kuljettu pysäkin läpi.


            //Pitää siis olla alussa Stop->List<Line>, jossa näkyy, millä linjoilla ei vielä ole matkustettu kyseisen pysäkin läpi. 

            Date = date;

            AllStops.AddRange(TransportDataGateway.GetStops());
            var initial = AllStops.First(s => s.Id == stopId);

            Visit(initial, date.Hour * 3600 + date.Minute * 60);

            while(SortedNodes.Count > 0)
            {
                CalculateNextRound();
            }

            return Visited.ToDictionary(p => p.Key, p => p.Value.Visited);
        }


        //Vaihtoehdot:
        /*
         - SortedNodessa ei olla käyty koskaan (Ei löydy kummastakaan)
         - SortedNodessa on käyty, ja käyntiaika on pienempi kuin tämä (Ei tehdä mitään)
         - SortedNodessa on käyty, ja käyntiaika on suurempi kuin tämä
            - Node on jonossa (Poistetaan jonosta ja lisätään uudella käyntiajalla)
            - Node ei ole jonossa (Lisätään jonoon)
             */

        void CalculateNextRound()
        {
            var nodeWithSmallestTime = SortedNodes.Min;

            SortedNodes.Remove(nodeWithSmallestTime);
            RemovedFromSorted.Add(nodeWithSmallestTime);
            HandleDepartures(nodeWithSmallestTime);
        }

        void HandleDepartures(StopNode node)
        {
            //Haetaan seuraavat lähdöt.
            var minimumDepartureTime = node.Visited + 1;
            var destinations = TransportDataGateway.GetDestinations(node.Stop, Date, minimumDepartureTime);

            foreach (var destination in destinations)
            {
                Visit(destination.Stop, destination.ArrivalTime);

            }
        }

        

        void Visit(Stop stop, int time)
        {
            if (!Visited.ContainsKey(stop))
            {
                VisitedIndex.Add(stop, VisitedIndex.Count);
                var node = AddNode(stop, time);
                Visited.Add(stop, node);
                
            }
            else if(Visited[stop].Visited > time)
            {
                var node = AddNode(stop, time);
                var old = Visited[stop];
                Visited[stop] = node;

                if (!RemovedFromSorted.Contains(old))
                {
                    SortedNodes.Remove(old);
                    RemovedFromSorted.Remove(old);
                }
            }

        }

        StopNode AddNode(Stop stop, int time)
        {
            var node = new StopNode(stop, time, VisitedIndex[stop]);
            SortedNodes.Add(node);
            return node;
        }
    }
}
