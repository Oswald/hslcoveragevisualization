﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Core
{
    public static class Logger
    {
        public static void Information(string log)
        {
            Console.WriteLine($"Information - {DateTime.Now.ToString()}: {log}");
        }
    }
}
