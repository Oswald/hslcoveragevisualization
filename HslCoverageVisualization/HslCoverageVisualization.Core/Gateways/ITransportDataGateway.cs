﻿using HslCoverageVisualization.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Core.Gateways
{
    public interface ITransportDataGateway
    {
        List<Stop> GetStops();
        List<Trip> GetDestinations(Stop stop, DateTime date, int minutes);
    }
}
