﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Contracts.Storage.Entities
{ 
    public class StopTimeData
    {
        public Guid StopId { get; }
        public Guid LineId { get; }
        public Guid TripId { get;}
        public int DepartureTime { get; }
        
        public StopTimeData(Guid stopId, Guid lineId, Guid tripId, int departureTime)
        {
            StopId = stopId;
            LineId = lineId;
            TripId = tripId;
            DepartureTime = departureTime;
        }
    }
}
