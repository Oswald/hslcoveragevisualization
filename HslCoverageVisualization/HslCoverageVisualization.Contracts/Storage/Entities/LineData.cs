﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Contracts.Storage.Entities
{
    public class LineData
    {
        public Guid Id { get; }
        public string Name { get; }

        public LineData(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
