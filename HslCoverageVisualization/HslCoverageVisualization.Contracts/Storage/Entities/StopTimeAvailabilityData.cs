﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Contracts.Storage.Entities
{
    public class StopTimeAvailabilityData
    {
        public Guid TripId { get; }
        public int Year { get; }
        public int Month { get; }
        public int Day { get; }

        public StopTimeAvailabilityData(Guid tripId, int year, int month, int day)
        {
            TripId = tripId;
            Year = year;
            Month = month;
            Day = day;
        }
    }
}
