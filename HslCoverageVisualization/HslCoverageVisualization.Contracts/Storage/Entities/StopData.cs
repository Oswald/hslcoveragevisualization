﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Contracts.Storage.Entities
{
    public class StopData
    {
        public Guid Id { get; }
        public string Name { get; }
        public double Latitude { get; }
        public double Longitude { get; }

        public StopData(Guid id, string name, double latitude, double longitude)
        {
            Id = id;
            Name = name;
            Latitude = latitude;
            Longitude = longitude;
        }
    }
}
