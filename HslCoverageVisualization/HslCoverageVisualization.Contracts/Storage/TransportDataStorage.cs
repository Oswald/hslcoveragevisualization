﻿using HslCoverageVisualization.Contracts.Storage.Entities;
using HslCoverageVisualization.Core;
using HslCoverageVisualization.Core.Gateways;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HslCoverageVisualization.Contracts.Storage
{
    public class TransportDataStorage
    {
        string ConnectionString { get; }
        public TransportDataStorage(string connectionString)
        {
            ConnectionString = connectionString;
        }

        /// <summary>
        /// Returns list of stop time data, ordered by 
        /// </summary>
        /// <param name="stopId"></param>
        /// <param name="date"></param>
        /// <param name="minutes"></param>
        /// <returns></returns>
        /// 
        public LinkedList<StopData> GetStops()
        {
            
            LinkedList<StopData> stops = new LinkedList<StopData>();

            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                connection.Open();

                var command = new NpgsqlCommand("SELECT * FROM \"Stop\";", connection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    stops.AddLast(new StopData(reader.GetFieldValue<Guid>(0), reader.GetFieldValue<string>(3), reader.GetFieldValue<double>(1), reader.GetFieldValue<double>(2)));
                }

                connection.Close();
            }

            return stops;
        }

        public LinkedList<StopTimeData> GetStopTimes(DateTime date)
        {
            LinkedList<StopTimeData> stopTimes = new LinkedList<StopTimeData>();

            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                connection.Open();

                var command = new NpgsqlCommand("SELECT * FROM \"StopTime\" WHERE \"TripId\" IN (SELECT \"TripId\" FROM \"StopTimeAvailability\" WHERE \"Date\" = @date);", connection);
                command.Parameters.AddWithValue("date", NpgsqlTypes.NpgsqlDbType.Date, new NpgsqlDate(date));
                var reader = command.ExecuteReader();



                while (reader.Read())
                {
                    stopTimes.AddLast(new StopTimeData(reader.GetFieldValue<Guid>(0), reader.GetFieldValue<Guid>(1), reader.GetFieldValue<Guid>(2), reader.GetFieldValue<int>(3)));
                }

                connection.Close();
            }

            return stopTimes;
        }

        public LinkedList<StopTimeData> GetStopTimes(Guid stopId, DateTime date, int minutes)
        {
            LinkedList<StopTimeData> stopTimes = new LinkedList<StopTimeData>();

            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                connection.Open();

                var command = new NpgsqlCommand("SELECT * FROM \"StopTime\" WHERE \"TripId\" IN (SELECT \"TripId\" FROM \"StopTime\" WHERE \"StopId\" = @stopId AND \"DepartureTime\" BETWEEN @time AND @maxTime AND \"TripId\" IN (SELECT \"TripId\" FROM \"StopTimeAvailability\" WHERE \"Date\" = @date)) AND \"DepartureTime\" >= @time;", connection);
                command.Parameters.AddWithValue("stopId", NpgsqlTypes.NpgsqlDbType.Uuid, stopId);
                command.Parameters.AddWithValue("date", NpgsqlTypes.NpgsqlDbType.Date, new NpgsqlDate(date));
                command.Parameters.AddWithValue("time", NpgsqlTypes.NpgsqlDbType.Integer, minutes);
                command.Parameters.AddWithValue("maxTime", NpgsqlTypes.NpgsqlDbType.Integer, minutes + 3600);
                command.Prepare();

                var reader = command.ExecuteReader();



                while (reader.Read())
                {
                    stopTimes.AddLast(new StopTimeData(reader.GetFieldValue<Guid>(0), reader.GetFieldValue<Guid>(1), reader.GetFieldValue<Guid>(2), reader.GetFieldValue<int>(3)));
                }

                connection.Close();
            }

            return stopTimes;
        }

        public void SaveStopTimes(List<StopTimeAvailabilityData> availabilities, List<StopTimeData> stopTimes)
        {
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                connection.Open();

                using (NpgsqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        Logger.Information("Saving stop times");
                        AddStopTimeData(connection, transaction, stopTimes);
                        Logger.Information("Saving availability data");
                        AddStopTimeAvailabilityData(connection, transaction, availabilities);

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw new Exception("Error when making queries. Rolling back transaction", e);
                    }
                }

                connection.Close();
            }
        }

        public void SaveStops(List<StopData> stops)
        {
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                connection.Open();

                using (NpgsqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        Logger.Information("Saving stops");
                        AddStops(connection, transaction, stops);

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw new Exception("Error when making queries. Rolling back transaction", e);
                    }
                }

                connection.Close();
            }
        }

        public void SaveLines(List<LineData> lines)
        {
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                connection.Open();

                using (NpgsqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        Logger.Information("Saving lines");
                        AddLines(connection, transaction, lines);
                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw new Exception("Error when making queries. Rolling back transaction", e);
                    }
                }
                connection.Close();
            }
        }

        void AddLines(NpgsqlConnection connection, NpgsqlTransaction transaction, List<LineData> lines)
        {
            foreach (var line in lines)
            {
                var command = new NpgsqlCommand("INSERT INTO \"Line\" VALUES (@id, @name);", connection, transaction);
                command.Parameters.AddWithValue("id", line.Id);
                command.Parameters.AddWithValue("name", line.Name);
                command.Prepare();


                if (command.ExecuteNonQuery() != 1)
                {
                    throw new Exception($"Could not add line: {command.ToString()}");
                }
            }
        }

        void AddStops(NpgsqlConnection connection, NpgsqlTransaction transaction, List<StopData> stops)
        {
            var command = new NpgsqlCommand("INSERT INTO \"Stop\" VALUES (@id, @latitude, @longitude, @name);", connection, transaction);
            command.Parameters.Add(new NpgsqlParameter("id", NpgsqlTypes.NpgsqlDbType.Uuid));
            command.Parameters.Add(new NpgsqlParameter("latitude", NpgsqlTypes.NpgsqlDbType.Double));
            command.Parameters.Add(new NpgsqlParameter("longitude", NpgsqlTypes.NpgsqlDbType.Double));
            command.Parameters.Add(new NpgsqlParameter("name", NpgsqlTypes.NpgsqlDbType.Varchar));

            foreach (var stop in stops)
            {
                command.Parameters[0].Value = stop.Id;
                command.Parameters[1].Value = stop.Latitude;
                command.Parameters[2].Value = stop.Longitude;
                command.Parameters[3].Value = stop.Name;
                command.Prepare();

                if (command.ExecuteNonQuery() != 1)
                {
                    throw new Exception($"Could not add stop: {command.ToString()}");
                }
            }

        }

        void AddStopTimeData(NpgsqlConnection connection, NpgsqlTransaction transaction, List<StopTimeData> stopTimes)
        {
            var command = new NpgsqlCommand("INSERT INTO \"StopTime\" VALUES (@stopId, @lineId, @tripId, @departureTime);", connection, transaction);
            command.Parameters.Add(new NpgsqlParameter("stopId", NpgsqlTypes.NpgsqlDbType.Uuid));
            command.Parameters.Add(new NpgsqlParameter("lineId", NpgsqlTypes.NpgsqlDbType.Uuid));
            command.Parameters.Add(new NpgsqlParameter("tripId", NpgsqlTypes.NpgsqlDbType.Uuid));
            command.Parameters.Add(new NpgsqlParameter("departureTime", NpgsqlTypes.NpgsqlDbType.Integer));

            foreach (var time in stopTimes)
            {
                command.Parameters[0].Value = time.StopId;
                command.Parameters[1].Value = time.LineId;
                command.Parameters[2].Value = time.TripId;
                command.Parameters[3].Value = time.DepartureTime;
                command.Prepare();

                if (command.ExecuteNonQuery() != 1)
                {
                    throw new Exception($"Could not add stoptime: {command.ToString()}");
                }
            }

        }

        void AddStopTimeAvailabilityData(NpgsqlConnection connection, NpgsqlTransaction transaction, List<StopTimeAvailabilityData> stopTimeAvailabilities)
        {
            var command = new NpgsqlCommand("INSERT INTO \"StopTimeAvailability\" VALUES (@tripId, @date);", connection, transaction);
            command.Parameters.Add(new NpgsqlParameter("tripId", NpgsqlTypes.NpgsqlDbType.Uuid));
            command.Parameters.Add(new NpgsqlParameter("date", NpgsqlTypes.NpgsqlDbType.Date));

            foreach (var availability in stopTimeAvailabilities)
            {
                command.Parameters[0].Value = availability.TripId;
                command.Parameters[1].Value = new NpgsqlDate(availability.Year, availability.Month, availability.Day);
                command.Prepare();

                if (command.ExecuteNonQuery() != 1)
                {
                    throw new Exception($"Could not add availability: {command.ToString()}");
                }
            }
        }
    }
}
