﻿using HslCoverageVisualization.Contracts.Storage;
using HslCoverageVisualization.Contracts.Storage.Entities;
using HslCoverageVisualization.Core.Entities;
using HslCoverageVisualization.Core.Gateways;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HslCoverageVisualization.Contracts.Gateways.TransportDataGateway
{
    public class CacheTransportDataGateway : ITransportDataGateway
    {
        TransportDataStorage Storage { get; }
        Dictionary<Guid, Stop> Stops { get; set; } = null;
        Dictionary<Guid, List<Trip>> TripsLeavingFromStop { get; set; } = new Dictionary<Guid, List<Trip>>();
        Dictionary<Guid, Dictionary<Stop, int>> WalkingDistancesFromStop { get; set; } = new Dictionary<Guid, Dictionary<Stop, int>>();
        public CacheTransportDataGateway(TransportDataStorage storage)
        {
            Storage = storage;
            Initialize(new DateTime(2019, 12, 4));
        }

        void Initialize(DateTime date)
        {
            Console.WriteLine("Preparing cache. Fething stops");
            var stops = Storage.GetStops();
            Stops = stops.ToDictionary(s => s.Id, s => new Stop(s.Id, s.Name, new Location(s.Latitude, s.Longitude)));

            Console.WriteLine("Fetching stoptimes");
            var stoptimes = Storage.GetStopTimes(date);

            SetTrips(stoptimes);
            SetWalkingDistances();

        }

        void SetTrips(IEnumerable<StopTimeData> stoptimes)
        {
            var trips = stoptimes.GroupBy(s => s.TripId);
            foreach (var trip in trips)
            {
                var times = trip.OrderBy(t => t.DepartureTime).ToList();

                for (int startpos = 0; startpos < times.Count() - 1; startpos++)
                {
                    for (int endpos = startpos + 1; endpos < times.Count; endpos++)
                    {
                        var start = times[startpos];
                        var end = times[endpos];

                        if (!TripsLeavingFromStop.ContainsKey(start.StopId))
                        {
                            TripsLeavingFromStop.Add(start.StopId, new List<Trip>());
                        }

                        TripsLeavingFromStop[start.StopId].Add(new Trip(Stops[end.StopId], start.DepartureTime, end.DepartureTime));
                    }
                }
            }
        }

        void SetWalkingDistances()
        {
            foreach (var start in Stops)
            {
                WalkingDistancesFromStop.Add(start.Key, new Dictionary<Stop, int>());

                foreach(var target in Stops)
                {
                    var diff = CalculateDistanceBetweenStops(start.Value, target.Value);

                    if (diff < 1800)
                    {
                        WalkingDistancesFromStop[start.Key].Add(target.Value, diff);
                    }
                }
            }
            
        }

        /// <summary>
        /// Returns the walking timw between two stops
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        int CalculateDistanceBetweenStops(Stop s1, Stop s2)
        {
            var l1 = s1.Location;
            var l2 = s2.Location;

            //Metres
            var latitudeDiff = (l1.Latitude - l2.Latitude) * 111195;
            var longitudeDiff = (l1.Longitude - l2.Longitude) * 55598;

            var distance = Pythagoras(latitudeDiff, longitudeDiff);

            //1 m/s speed
            return (int)Math.Round(distance, 0);

        }

        double Pythagoras(double x, double y)
        {
            return Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
        }

        public List<Stop> GetStops()
        {
            return Stops.Values.ToList();
        }

        public List<Trip> GetDestinations(Stop departureStop, DateTime date, int minutes)
        {
            
            if (TripsLeavingFromStop.ContainsKey(departureStop.Id))
            {
                var trips = TripsLeavingFromStop[departureStop.Id].Where(t => t.DepartureTime >= minutes).ToList();
                var walking = WalkingDistancesFromStop[departureStop.Id].Select(s => new Trip(s.Key, minutes, minutes + s.Value));
                trips.AddRange(walking);
                return trips;
            }
            else
            {
                return new List<Trip>();
            }

        }
    }
}
