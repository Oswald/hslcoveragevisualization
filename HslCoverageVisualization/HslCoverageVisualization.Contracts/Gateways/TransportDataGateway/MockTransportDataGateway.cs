﻿using HslCoverageVisualization.Core.Entities;
using HslCoverageVisualization.Core.Gateways;
using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Contracts.Gateways.TransportDataGateway
{
    class MockTransportDataGateway : ITransportDataGateway
    {
        public List<Trip> GetDestinations(Stop stop, DateTime date, int minutes)
        {
            throw new NotImplementedException();
        }

        public List<Stop> GetStops()
        {
            throw new NotImplementedException();
        }
    }
}
