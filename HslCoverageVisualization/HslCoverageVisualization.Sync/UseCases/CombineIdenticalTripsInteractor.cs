﻿using HslCoverageVisualization.Sync.TransportDataGateway.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HslCoverageVisualization.Sync.UseCases
{
    static class CombineIdenticalTripsInteractor
    {
        public static void Handle(RouteScheduleDto route)
        {
            foreach(var pattern in route.patterns)
            {
                pattern.trips = CombineTrips(pattern.trips);
            }
        }

        static List<TripDto> CombineTrips(List<TripDto> trips)
        {
            List<TripDto> newTrips = new List<TripDto>();

            foreach(var trip in trips)
            {
                var identical = FindIdentical(trip, newTrips);

                if(identical == null)
                {
                    newTrips.Add(trip);
                }
                else
                {
                    identical.activeDates.AddRange(trip.activeDates);
                }
            }

            foreach(var trip in newTrips)
            {
                trip.activeDates = trip.activeDates.Distinct().ToList();
            }
            return newTrips;
        }

        /// <summary>
        /// Returns new trip that has identical stopTimes. If no identical is found, returns null
        /// </summary>
        static TripDto FindIdentical(TripDto trip, List<TripDto> newTrips)
        {
            return newTrips.FirstOrDefault(t => AreIdentical(trip, t));
        }

        static bool AreIdentical(TripDto trip1, TripDto trip2)
        {
            if(trip1.stoptimes.Count != trip2.stoptimes.Count)
            {
                return false;
            }
            for(int i = 0; i < trip1.stoptimes.Count; i++)
            {
                if(trip1.stoptimes[1].scheduledArrival != trip1.stoptimes[2].scheduledArrival)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
