﻿using HslCoverageVisualization.Contracts.Storage;
using HslCoverageVisualization.Contracts.Storage.Entities;
using HslCoverageVisualization.Sync.TransportDataGateway;
using HslCoverageVisualization.Sync.TransportDataGateway.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HslCoverageVisualization.Sync.UseCases
{
    class TransferTransportDataInteractor
    {
        SaveTransportDataInteractor SaveTransport { get; }
        TransportDataStorage TransportDataStorage { get; }
        HslGateway HslGateway { get; }
        public TransferTransportDataInteractor(TransportDataStorage transportDataStorage, HslGateway hslGateway)
        {
            SaveTransport = new SaveTransportDataInteractor(transportDataStorage);
            TransportDataStorage = transportDataStorage;
            HslGateway = hslGateway;
        }

        public async Task Handle()
        {
            var lines = HslGateway.GetLines().Result;
            Logger.Information("Fetching stops");
            var stops = HslGateway.GetStops().Result;
            Logger.Information("Fetching scedules");

            Dictionary<string, LineData> linesWithOriginalId = lines
                .ToDictionary(
                    l => l.gtfsId,
                    l => new LineData(Guid.NewGuid(), l.shortName));

            Dictionary<string, StopData> stopsWithOriginalId = stops
                .ToDictionary(
                    l => l.gtfsId,
                    l => new StopData(Guid.NewGuid(), l.name, l.lat, l.lon));

            var schedules = new List<RouteScheduleDto>();

            TransportDataStorage.SaveLines(linesWithOriginalId.Values.ToList());
            TransportDataStorage.SaveStops(stopsWithOriginalId.Values.ToList());

            var failedLines = new List<LineDto>();

            var gatewaySemaphore = new Semaphore(2, 2);
            var storageSemaphore = new Semaphore(1, 1);

            var tasks = new List<Task>();

            foreach (var line in lines)
            {
                tasks.Add(TransferLine(linesWithOriginalId, stopsWithOriginalId, line, failedLines, gatewaySemaphore, storageSemaphore));
            }

            Task.WaitAll(tasks.ToArray());

            foreach (var line in failedLines)
            {
                Console.WriteLine(line.shortName);
            }
        }

        async Task TransferLine(Dictionary<string, LineData> linesWithOriginalId, Dictionary<string, StopData> stopsWithOriginalId, LineDto line, List<LineDto> failedLines, Semaphore gatewaySemaphore, Semaphore storageSemaphore)
        {
            gatewaySemaphore.WaitOne();

            try
            {

                Logger.Information($"Fetching {line.shortName}");
                var schedule = await HslGateway.GetSchedules(line);

                CombineIdenticalTripsInteractor.Handle(schedule);

                storageSemaphore.WaitOne();

                try
                {
                    SaveTransport.Handle(linesWithOriginalId, stopsWithOriginalId, schedule);
                }
                catch (Exception e)
                {
                    throw new Exception("Could not save line.", e);
                }
                finally
                {
                    storageSemaphore.Release();
                }
            }
            catch (Exception e)
            {
                failedLines.Add(line);
                Logger.Error($"Could not transfer line {line.shortName} (id: {line.gtfsId}). \r\nReason: {e.ToString()}");
            }
            finally
            {
                gatewaySemaphore.Release();
            }
        }
    }
}
