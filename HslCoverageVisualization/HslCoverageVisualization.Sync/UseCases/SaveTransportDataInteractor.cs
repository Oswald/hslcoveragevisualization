﻿using HslCoverageVisualization.Contracts.Storage;
using HslCoverageVisualization.Contracts.Storage.Entities;
using HslCoverageVisualization.Sync.TransportDataGateway.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HslCoverageVisualization.Sync.UseCases
{
    class SaveTransportDataInteractor
    {
        TransportDataStorage Storage { get; }
        public SaveTransportDataInteractor(TransportDataStorage storage)
        {
            Storage = storage;
        }

        public void Handle(Dictionary<string, LineData> linesWithOriginalId, Dictionary<string, StopData> stopsWithOriginalId, RouteScheduleDto route)
        {
            List<StopTimeData> stopTimeData = new List<StopTimeData>();
            List<StopTimeAvailabilityData> stopTimeAvailabilityDates = new List<StopTimeAvailabilityData>();
            Logger.Information($"Converting route {route.shortName}");

            foreach (var pattern in route.patterns)
            {
                foreach (var trip in pattern.trips)
                {
                    var tripId = Guid.NewGuid();

                    foreach (var availability in trip.activeDates)
                    {
                        stopTimeAvailabilityDates.Add(new StopTimeAvailabilityData(tripId, ParseYear(availability), ParseMonth(availability), ParseDay(availability)));
                    }

                    foreach(var stoptime in trip.stoptimes)
                    {
                        stopTimeData.Add(new StopTimeData(stopsWithOriginalId[stoptime.stop.gtfsId].Id, linesWithOriginalId[route.gtfsId].Id, tripId, stoptime.scheduledArrival));
                    }
                }
            }
            
            Logger.Information($"Saving route {route.shortName} to storage");
            Storage.SaveStopTimes(stopTimeAvailabilityDates, stopTimeData);


        }

        int ParseYear(string date)
        {
            return Int32.Parse(date.Substring(0, 4));
        }

        int ParseMonth(string date)
        {
            return Int32.Parse(date.Substring(4, 2));
        }

        int ParseDay(string date)
        {
            return Int32.Parse(date.Substring(6, 2));
        }




    }
}
