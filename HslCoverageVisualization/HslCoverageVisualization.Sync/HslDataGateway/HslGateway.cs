﻿using GraphQL.Client;
using GraphQL.Common.Request;
using HslCoverageVisualization.Sync.TransportDataGateway.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HslCoverageVisualization.Sync.TransportDataGateway.Converters;
using System.Net.Http;
using System.Linq;

namespace HslCoverageVisualization.Sync.TransportDataGateway
{
    class HslGateway
    {
        GraphQLClient Client { get; }
        HttpClient HttpClient { get; }
        public HslGateway(Uri baseUrl)
        {
            Client = new GraphQLClient(baseUrl);
            HttpClient = new HttpClient();
            HttpClient.Timeout = new TimeSpan(0, 10, 0);
            HttpClient.BaseAddress = baseUrl;
        }

        public async Task<List<LineDto>> GetLines()
        {
            var query = new GraphQLRequest
            {
                Query = @"
	            {
		            routes {
			            shortName
                        longName
                        gtfsId
		            }
	            }"
            };

            var graphQLResponse = await Client.PostAsync(query);
            var routes = (JArray)graphQLResponse.Data["routes"];
            return LineConverter.Convert(routes);
        }

        public async Task<List<StopDto>> GetStops()
        {
            var query = new GraphQLRequest
            {
                Query = @"
	            {
		            stops {
			            gtfsId
                        name
                        lat
                        lon
		            }
	            }"
            };

            var graphQLResponse = await Client.PostAsync(query);
            var routes = (JArray)graphQLResponse.Data["stops"];
            return StopConverter.Convert(routes);
        }

        public async Task<RouteScheduleDto> GetSchedules(LineDto line)
        {

            var msg = new HttpRequestMessage();
            msg.Method = HttpMethod.Post;
            msg.Content = new StringContent("{ route(id: \"" + line.gtfsId + "\") { shortName longName gtfsId patterns { code directionId name headsign trips { gtfsId activeDates stoptimes{ scheduledArrival stop { name gtfsId } } } } } }");
            msg.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/graphql");
            var response = await HttpClient.SendAsync(msg);


            if(response.StatusCode == System.Net.HttpStatusCode.GatewayTimeout)
            {
                return await GetSchedulesInPieces(line);
            }

            var content = await response.Content.ReadAsStringAsync();
            try
            {
                var data = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(content);
                return data["data"]["route"].ToObject<RouteScheduleDto>();
            }
            catch (Exception e)
            {
                throw new Exception($"Could not parse content. Status code: {response.StatusCode}");
            }
        }

        public async Task<RouteScheduleDto> GetSchedulesInPieces(LineDto line)
        {
            var msg = new HttpRequestMessage();
            msg.Method = HttpMethod.Post;
            msg.Content = new StringContent("{ route(id: \"" + line.gtfsId + "\") { shortName longName gtfsId patterns { code directionId name headsign trips { gtfsId activeDates } } } }");
            msg.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/graphql");

            var response = await HttpClient.SendAsync(msg);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Could not fetch line data");
            }

            var content = await response.Content.ReadAsStringAsync();
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(content);
            var route = data["data"]["route"].ToObject<RouteScheduleDto>();

            foreach(var pattern in route.patterns)
            {
                foreach(var trip in pattern.trips)
                {
                    trip.stoptimes = await GetStopTimesWithId(trip.gtfsId);
                }
            }

            return route;
        }

        public async Task<List<StopTimeDto>> GetStopTimesWithId(string tripId)
        {
            var msg = new HttpRequestMessage();
            msg.Method = HttpMethod.Post;
            msg.Content = new StringContent("{ trip(id: \"" + tripId + "\") { stoptimes{ scheduledArrival stop { name gtfsId } } } } ");
            msg.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/graphql");

            var response = await HttpClient.SendAsync(msg);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Could not fetch stoptimes for trip: {tripId}");
            }

            var content = await response.Content.ReadAsStringAsync();
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(content);
            var route = data["data"]["trip"].ToObject<TripDto>();
            return route.stoptimes;
        }
    }
}
