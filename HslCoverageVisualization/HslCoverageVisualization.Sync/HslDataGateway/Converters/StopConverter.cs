﻿using HslCoverageVisualization.Sync.TransportDataGateway.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HslCoverageVisualization.Sync.TransportDataGateway.Converters
{
    static class StopConverter
    {
        public static List<StopDto> Convert(JArray input)
        {
            return input.Select(t => ConvertStop(t)).ToList();
        }

        static StopDto ConvertStop(JToken input)
        {
            return new StopDto
            {
                gtfsId = input.Value<string>("gtfsId"),
                lat = input.Value<double>("lat"),
                lon = input.Value<double>("lon"),
                name = input.Value<string>("name")
            };
        }
    }
}
