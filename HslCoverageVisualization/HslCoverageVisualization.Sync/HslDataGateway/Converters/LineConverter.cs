﻿using HslCoverageVisualization.Sync.TransportDataGateway.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HslCoverageVisualization.Sync.TransportDataGateway.Converters
{
    static class LineConverter
    {
        public static List<LineDto> Convert(JArray input)
        {
            return input.Select(t => ConvertLine(t)).ToList();
        }

        static LineDto ConvertLine(JToken input)
        {
            return new LineDto { gtfsId = input.Value<string>("gtfsId"), shortName = input.Value<string>("shortName") };
        }
    }
}
