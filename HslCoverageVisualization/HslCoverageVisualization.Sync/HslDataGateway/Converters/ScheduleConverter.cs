﻿using HslCoverageVisualization.Sync.TransportDataGateway.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HslCoverageVisualization.Sync.TransportDataGateway.Converters
{
    static class ScheduleConverter
    {
        public static List<RouteScheduleDto> Convert(JArray input)
        {
            return input.Select(t => ConvertRoute(t)).ToList();
        }

        static RouteScheduleDto ConvertRoute(JToken input)
        {
            return new RouteScheduleDto
            {
                gtfsId = input.Value<string>("gtfsId"),
                shortName = input.Value<string>("shortName"),
                patterns = input.Value<JArray>("patterns").Select(t => ConvertPattern(t)).ToList()
            };
        }

        static PatternDto ConvertPattern(JToken input)
        {
            return new PatternDto
            {
                code = input.Value<string>("code"),
                directionId = input.Value<int>("directionId"),
                trips = input.Value<JArray>("trips").Select(t => ConvertTrip(t)).ToList()
            };
        }

        static TripDto ConvertTrip(JToken input)
        {
            return new TripDto
            {
                gtfsId = input.Value<string>("gtfsId"),
                activeDates = input.Value<JArray>("activeDates").Select(t => t.Value<string>()).ToList(),
                stoptimes = input.Value<JArray>("stoptimes").Select(t => ConvertStopTime(t)).ToList()
            };
        }

        static StopTimeDto ConvertStopTime(JToken input)
        {
            return new StopTimeDto
            {
                scheduledArrival = input.Value<int>("scheduledArrival"),
                stop = ConvertStopInfo(input.Value<JToken>("stop"))
            };
        }

        static StopInfoDto ConvertStopInfo(JToken input)
        {
            return new StopInfoDto
            {
                gtfsId = input.Value<string>("gtfsId"),
                name = input.Value<string>("name")
            };
        }
    }
}

