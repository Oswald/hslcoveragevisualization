﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Sync.TransportDataGateway.Dto
{
    class RouteScheduleDto
    {
        public string gtfsId { get; set; }
        public string shortName { get; set; }
        public List<PatternDto> patterns { get; set; }
    }

    class PatternDto
    {
        public string code { get; set; }
        public int directionId { get; set; }
        public List<TripDto> trips { get; set; }
    }

    class TripDto
    {
        public string gtfsId { get; set; }
        public List<string> activeDates { get; set; }
        public List<StopTimeDto> stoptimes { get; set; }
    }

    class StopTimeDto
    {
        public int scheduledArrival { get; set; }
        public StopInfoDto stop { get; set; }
    }

    class StopInfoDto
    {
        public string name { get; set; }
        public string gtfsId { get; set; }
    }
}
