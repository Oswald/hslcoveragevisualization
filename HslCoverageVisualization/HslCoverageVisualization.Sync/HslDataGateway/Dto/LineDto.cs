﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Sync.TransportDataGateway.Dto
{
    class LineDto
    {
        public string gtfsId { get; set; }
        public string shortName { get; set; }
    }
}
