﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HslCoverageVisualization.Sync.TransportDataGateway.Dto
{
    class StopDto
    {
        public string gtfsId { get; set; }
        public string name { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
    }
}
