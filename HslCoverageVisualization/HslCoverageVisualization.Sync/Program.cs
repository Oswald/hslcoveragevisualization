﻿using HslCoverageVisualization.Contracts.Storage;
using HslCoverageVisualization.Sync.TransportDataGateway;
using HslCoverageVisualization.Sync.TransportDataGateway.Dto;
using HslCoverageVisualization.Sync.UseCases;
using System;
using System.Collections.Generic;

namespace HslCoverageVisualization.Sync
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.Information("Starting sync");
            var CacheTransportDataGateway = new HslGateway(new Uri("https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql"));

            var storageGateway = new TransportDataStorage("Server=192.168.1.3;Port=5432;User Id=hsl;Password=L978y93gfasfhwq;Database=hsl;");

            var transfer = new TransferTransportDataInteractor(storageGateway, CacheTransportDataGateway);

            transfer.Handle().Wait();

            Logger.Information("Sync completed");
        }
    }
}
