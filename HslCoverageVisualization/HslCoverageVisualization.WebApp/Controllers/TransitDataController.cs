﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HslCoverageVisualization.Core.Gateways;
using HslCoverageVisualization.Core.UseCases;
using HslCoverageVisualization.WebApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HslCoverageVisualization.WebApp.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TransitDataController : ControllerBase
    {
        ITransportDataGateway TransportDataGateway { get; }
        public TransitDataController(ITransportDataGateway cacheTransportDataGateway)
        {
            TransportDataGateway = cacheTransportDataGateway;
        }
        public List<StopTimeModel> StopTimes(string stop, string time)
        {
            var interactor = new CalculateTransportTimesToStops(TransportDataGateway);
            var data = interactor.Handle(DateTime.Parse(time, System.Globalization.CultureInfo.InvariantCulture), Guid.Parse(stop));
            return data.Select(s => new StopTimeModel { id = s.Key.Id, latitude = s.Key.Location.Latitude, longitude = s.Key.Location.Longitude, name = s.Key.Name, time = s.Value }).ToList();
        }

        public List<StopModel> Stops()
        {
            var interactor = new ListAllStopsInteractor(TransportDataGateway);
            var data = interactor.Handle();
            return data.Select(s => new StopModel { id = s.Id, name = s.Name, latitude = s.Location.Latitude, longitude = s.Location.Longitude }).ToList();
        }

        public List<StopTimeModel> Mock()
        {
            var text = System.IO.File.ReadAllText("mockData2.json");
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StopTimeModel>>(text);
            return data;
        }
    }
}