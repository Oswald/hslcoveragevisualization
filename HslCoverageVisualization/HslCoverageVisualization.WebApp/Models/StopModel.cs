﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HslCoverageVisualization.WebApp.Models
{
    public class StopModel
    {
        public Guid id;
        public string name;
        public double latitude;
        public double longitude;
    }
}
